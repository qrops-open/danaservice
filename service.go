package danaservice

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"

	"gitlab.com/qrops-open/configuration"
)

const POST string = "POST"
const GET string = "GET"

//Request is struct to hold service request
type Request struct {
	Request Child  `json:"request"`
	Sign    string `json:"signature"`
}

// Response is struct to hold service response
type Response struct {
	Response Child  `json:"response"`
	Sign     string `json:"signature"`
}

// Child is the request of service
type Child struct {
	Head Head `json:"head"`
	Body Body `json:"body"`
}

// Head of the request or response
type Head struct {
	Version      string `json:"version"`
	Function     string `json:"function"`
	ClientID     string `json:"clientId"`
	ClientSecret string `json:"clientSecret"`
	ReqTime      string `json:"reqTime"`
	ReqMsgID     string `json:"reqMsgId"`
	AccessToken  string `json:"accessToken"`
}

//Body of the request or response
type Body interface{}

//Shop to reflect shop fields
type Shop struct {
	MerchantID       string         `json:"merchantId,omitempty"`
	ShopID           string         `json:"shopId,omitempty"`
	ShopIDType       string         `json:"shopIdType,omitempty"`
	ShopParentType   string         `json:"shopParentType,omitempty"`
	MainName         string         `json:"mainName,omitempty"`
	ExternalShopID   string         `json:"externalShopId,omitempty"`
	MccCodes         []string       `json:"mccCodes,omitempty"`
	SizeType         string         `json:"sizeType,omitempty"`
	Longitude        string         `json:"ln,omitempty"`
	Latitude         string         `json:"lat,omitempty"`
	ShopBusinessType string         `json:"shopBizType,omitempty"`
	ExtInfo          *ExtensionInfo `json:"extInfo,omitempty"`
	ShopAddress      *Address       `json:"shopAddress,omitempty"`
}

type ExtensionInfo struct {
	Phone1           string `json:"PHONE_1,omitempty"`
	Phone2           string `json:"PHONE_2,omitempty"`
	Loyalty          string `json:"loyalty,omitempty"`
	DeviceNumber     string `json:"DEVICE_NUMBER,omitempty"`
	PosNumber        string `json:"POS_NUMBER,omitempty"`
	ShopBusinessType string `json:"SHOP_BIZ_TYPE,omitempty"`
}

// Address to represent address
type Address struct {
	Address1 string `json:"address1,omitempty"`
	City     string `json:"city,omitempty"`
}

//LogoURL to represent logo
type LogoURL struct {
	Logo       string `json:"LOGO,omitempty"`
	PcLogo     string `json:"PC_LOGO,omitempty"`
	MobileLogo string `json:"MOBILE_LOGO,omitempty"`
}

// ResultInfo is a response to indicate whether the call is successful or not.
type ResultInfo struct {
	ResultStatus string `json:"resultStatus"`
	ResultCodeID string `json:"resultCodeId"`
	ResultCode   string `json:"resultCode"`
	ResultMsg    string `json:"resultMsg"`
}

//ResponseBody is body of the response
type ResponseBody struct {
	ResultInfo ResultInfo `json:"resultInfo"`
}

var signer Signer

//getUUID is a function that returns uuid
func getUUID() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	uuid := fmt.Sprintf("%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return uuid
}

// NewHead is a constructor of Head
func NewHead(function string, config configuration.ConfigRepository) Head {
	head := Head{
		Version:      "2.0",
		Function:     function,
		ClientID:     config.GetConfig("CLIENT_ID"),
		ClientSecret: config.GetConfig("CLIENT_SECRET"),
		AccessToken:  "234567a",
		ReqMsgID:     getUUID(),
		ReqTime:      time.Now().Format(time.RFC3339),
	}
	return head
}

//loadPrivateKey to get the private key from file
func loadPrivateKey(path string) (Signer, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(b)

	key, _ := x509.ParsePKCS8PrivateKey(block.Bytes)
	return newSignerFromKey(key)
}

func newSignerFromKey(k interface{}) (Signer, error) {
	var sshKey Signer
	switch t := k.(type) {
	case *rsa.PrivateKey:
		sshKey = &rsaPrivateKey{t}
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %T", k)
	}
	return sshKey, nil
}

// A Signer is can create signatures that verify against a public key.
type Signer interface {
	// Sign returns raw signature for the given data. This method
	// will apply the hash specified for the keytype to the data.
	Sign(data []byte) ([]byte, error)
}

type rsaPrivateKey struct {
	*rsa.PrivateKey
}

// Sign signs data with rsa-sha256
func (r *rsaPrivateKey) Sign(data []byte) ([]byte, error) {
	d := sha256.Sum256(data)
	b := d[:]
	return rsa.SignPKCS1v15(rand.Reader, r.PrivateKey, crypto.SHA256, b)
}

var client *http.Client

//CallDanaService return response and error
func CallDanaService(shopService Request, endpoint string, config configuration.ConfigRepository) (Response, error) {
	var err error
	if signer == nil {
		signer, err = loadPrivateKey(config.GetConfig("Certificate_Private_Key"))
		failOnError(err, "Error on load private key")
	}

	str, err := json.Marshal(shopService.Request)
	if err != nil {
		log.Printf("Error occured on marshall json: %s\n", err)
		return Response{}, err
	}

	signed, _ := signer.Sign([]byte(str))

	sig := base64.StdEncoding.EncodeToString(signed)
	shopService.Sign = sig

	req, err := json.Marshal(shopService)
	failOnError(err, "Error")

	body, err := CallService(req, endpoint, config, POST)
	failOnError(err, "Error on call service")

	response := Response{
		Response: Child{
			Body: ResponseBody{},
		},
	}

	err = json.Unmarshal(body, &response)
	failOnError(err, "Error")
	return response, err
}

// CallService to call service
func CallService(req []byte, endpoint string, config configuration.ConfigRepository, method string) ([]byte, error) {

	log.Printf("[%s] Called Service at %s \n", method, endpoint)
	if req != nil {
		log.Printf("Request: %s\n", string(req))
	}

	if client == nil {
		tr := &http.Transport{
			MaxIdleConns: 10,
			Dial: (&net.Dialer{
				Timeout: 15 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 0,
		}
		client = &http.Client{
			Timeout:   time.Second * 30,
			Transport: tr,
		}
	}

	var resp *http.Response
	var err error

	switch method {
	case POST:
		resp, err = client.Post(endpoint, "application/json", bytes.NewBuffer(req))
	default:
		resp, err = client.Get(endpoint)
	}

	if err != nil {
		log.Printf("Error occured on calling service: %s\n", err)
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error occured on reading response: %s\n", err)
		return nil, err
	}
	resp.Body.Close()

	log.Printf("Response: %s\n", string(body))
	return body, err
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", err, msg)
	}
}
