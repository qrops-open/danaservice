package danaservice

import (
	"testing"

	"gitlab.com/qrops-open/configuration"
)

func newTestConfigFile() []byte {
	config := `{"CLIENT_ID": "1234567", "CLIENT_SECRET": "SECRET"}`
	return []byte(config)
}

func TestNewHead(t *testing.T) {
	config := configuration.NewConfigRepository(newTestConfigFile())
	head := NewHead("test.function", *config)

	if head.ClientID != "1234567" {
		t.Fatalf("Client ID should be %s, but got %s instead.", "1234567", head.ClientID)
	}

	if head.ClientSecret != "SECRET" {
		t.Fatalf("Client Secret should be %s, but got %s instead.", "SECRET", head.ClientSecret)
	}

	if head.Version != "2.0" {
		t.Fatalf("Version should be %s, but got %s instead.", "2.0", head.Version)
	}

	if head.AccessToken != "234567a" {
		t.Fatalf("Access Token should be %s, but got %s instead.", "234567a", head.AccessToken)
	}

	if head.Function != "test.function" {
		t.Fatalf("Function should be %s, but got %s instead.", "test.function", head.Function)
	}

}
